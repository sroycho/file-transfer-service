#!/usr/bin/env python3
import sys, os, time

#code adapted from UMBC python code examples
def fileinfo(files):
    maxlength = str(len(max(files, key=len)))
    # Use of ^ for centering and the use of a nested {}
    # within a format() call ~   {:>12{}}
    fmt = "{:" + maxlength + "} {:^9} {:^9} {:^9} {:>12{}}"
    pieces = ("Name:", "Exists?", "File?", "Dir?",
              "Size(bytes)", "")
    # Use of *pieces to unpack pieces into an argument list
    print(fmt.format(*pieces))
    for afile in files:
        print(fmt.format(afile, str(os.path.exists(afile)),
                         str(os.path.isfile(afile)),
                         str(os.path.isdir(afile)),
                         os.path.getsize(afile), ","))
        
def allstats(afile):
    tag = ["mode", "inode#", "device#", "#links", "user",
           "group", "bytes", "last access", "last modified",
           "change/creation time"]
    print("File Stats for:", afile)
    stats = os.stat(afile)
    fmt = "{:>22} : {}"
    for i, stat in enumerate(stats):
        print(fmt.format(tag[i], stat))
    
def datestats(afile):
    print("More Stats for:", afile)
    stats = os.stat(afile)
    print("Last Access:", time.ctime(stats.st_atime))
    print("Last Modified:", time.ctime(stats.st_mtime))
    print("Last Change:", time.ctime(stats.st_ctime))
    
def main():
    f = open('output.txt', 'w')
    print("Type of stream:", type(f).__name__)
    print("Module it is found in:", type(f).__module__)
    f.write('This is a test.\n') 
    f.write('This is another test.\n') 
    f.close()
    cwd = os.getcwd()
    print("Current Directory:", cwd)
    newdir = "afolder/asubfolder"
    os.makedirs(newdir, exist_ok=True)
    
    #get list of files and get info about first 5
    filelist = sorted(os.listdir(cwd))[:10]
    fileinfo(filelist)
    
    #get stats on a file
    allstats(sys.argv[0])
    datestats(sys.argv[0])

if __name__ == "__main__": main()
