#!usr/bin/env python3
import ipaddress
import re
import socket
import struct
import sys

def main():
    charCount = lineCount = 0
    with open(input("Enter a file name: "), "r") as aFile:
        while True:
            txt = aFile.readline()
            if not txt:
                break
            charCount += len(txt)
            lineCount += 1

    print("Characters:", charCount, " Lines:", lineCount)


"""
print("The choices for packet types are:")
print("1) Request Packet Type: C2 to File Transfer")
print("2) Initiate Packet Type: File Transfer Service to File Storage")
print("3) Response Packet Type: File Storage Service Response to File Transfer Service")
print("4) Validate Packet Type: File Transfer Service Response to C2")

packType = int(input("Please enter the packet type(1, 2, 3 or 4): "))
if packType < 1 or packType > 4:
	print("Please enter a number from 1-4")
	sys.exit()
print(packType)	
destIP   = input("Please enter the destination IP address (File Storage Service): ")
#from: https://stackoverflow.com/questions/10086572/ip-address-validation-in-python-using-regex
#Accessed: 08/24/17
#Author: Ranga
try:
	socket.inet_aton(destIP)
	#legal
except socket.error:
	print("Please enter a valid IP address in the range of 0.0.0.0 - 255.255.255.255")

print(destIP)
destPort = int(input("Please enter the destination Port: "))
if destPort < 16000 or destPort > 17000:
	print("Please enter a Port number in the range 16000-17000")
	sys.exit()
print(destPort)
encryptPatt  = input("Please enter the encryption pattern: ")
print(encryptPatt)
encryptPhrase = input("Please enter the encryption phrase: ")
print(encryptPhrase)
"""

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)#, socket.IPPROTO_RAW)
#data = 'string'
#sport = 4711    # arbitrary source port
#dport = 16001   # arbitrary destination port
#length = 8+len(data);
#checksum = 0
packType      = 1
destIP        = int(ipaddress.IPv4Address('127.0.0.1'))
destPort      = 16002
encLen        = 11
encryptPatt   = "^10:20;~:20".encode()
encryptPhrase = "This is the secret encryption phrase".encode()
reqPack = struct.pack('!bxxxIHH11s36s', packType, destIP, destPort, encLen, encryptPatt, encryptPhrase)
s.sendto(reqPack, ("127.0.0.1", 16002));

"""#https://tutorialedge.net/post/python/udp-client-server-python/
#Accessed 08/23/2017
#Create a UDP socket
udpIPAddr  = "127.0.0.1"
udpPortNum = 16000
msg        = "Test message"

clientSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#send the message
clientSock.sendto(b"Test message", ("127.0.0.1", 16000))"""

if __name__ == "__main__": main()
