#ifndef STRUCTS_H
#define STRUCTS_H

#include <arpa/inet.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
    
//https://stackoverflow.com/questions/1680365/integer-to-ip-address-c
//Author: Wernsey
//Accessed: 09/14/2017
void print_ip(int ip)
{
	unsigned char bytes[4];
	bytes[0] = ip & 0xFF;
	bytes[1] = (ip >> 8) & 0xFF;
	bytes[2] = (ip >> 16) & 0xFF;
	bytes[3] = (ip >> 24) & 0xFF;   
	printf("%d.%d.%d.%d\n", bytes[3], bytes[2], bytes[1], bytes[0]);        
}
 
    struct udpHeader
    {
        uint16_t srcPort;
        uint16_t destPort;
        uint16_t length;
        uint16_t checksum;
    };
    
	struct reqPack
	{
		uint8_t packType;
		uint32_t destIP;
		uint16_t destPort;
		uint16_t encLen;
		char     encryptPatt[13];
		//char	 encryptPhrase[];
	};

	struct initPacket
	{
		uint8_t packType;
	};    
	
	struct respPacket
	{
		uint8_t packType;
		uint16_t connPort;
	};

	struct valdtPacket
	{
		uint8_t packType;
	};
    

#endif
