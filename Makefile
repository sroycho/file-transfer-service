CFLAGS+=-std=c11 -D_GNU_SOURCE
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline -fstack-usage

LDLIBS+=-lm -pthread

all: mts #udpServer #server

mts: mts.o
#udpServer: udpServer.o
#server: server.o

.PHONY: clean debug profile

clean:
	-rm *.o *.su mts #udpServer #server

debug: CFLAGS+=-g
debug: mts #udpServer #server

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: mts #udpServer #server
     





















































